---
title: Resources Documentation
subtitle: 
comments: false
---


# Resources Documentation

This is a potential landing spot for Alma documentation.

1. If record is in PCI/SFX, they're not kept in catalog (even for existing records) (*via Metadata Working Group*).
   